from itertools import *

"""
Существует 10 рабочих (пронумерованы 0-9) и восемь задач (пронумерованы 0-7).
Стоимость распределения работников по задачам показана в таблице.
Задача состоит в том, чтобы назначить каждому работнику максимум одну задачу, 
при этом никакие два работника не выполняли бы одну и ту же задачу, при минимизации общей стоимости.
"""

work_table =[
    [90.27, 80.37, 75.74, 70.27, 89.24, 65.45, 54.17, 65.13],
    [35.56, 85.96, 55.45,65.45,65.66,78.39,33.28,34.14],
    [125.78,95.95,90.69,95.44,45.37,34.28,88.34,54.27],
    [45.32,110.27,95.10,115.41,54.51,46.44,77.19,66.39],
    [50.49,100.20,90.17,100.33,78.26,23.78,78.11,99.48],
    [100.10,90.35,100.18,50.55,90.21,45.98,87.18,85.25],
    [115.32,95.47,110.19,45.85,80.27,78.67,96.34,75.96],
    [95.44,90.10,95.57,125.47,70.29,32.64,99.59,65.43],
    [65.22,55.28,85.29,35.38,60.17,105.45,95.47,88.32],
    [70.84,75.36,80.38,90.21,78.12,97.26,93.29,78.28],
 ]

def get_all_positions(n):
    """ Получаем возможные позиции в которых каждому
    работнику достанется только одна задача, и не будет преесечений"""
    res = set()		
    # Рекурсия
    def find_pos(row_index, worker, n):
        for j in range(n):
            if pos_available(worker, (row_index, j)):
                worker[row_index] = j
                if row_index == n-1:   
                    res.add(tuple(worker))
                    return
                else:   # Нет до последней строки
                    find_pos(row_index+1, worker, n)
    
    def pos_available(worker, pos):
        for i in range(pos[0]):
            if worker[i] == pos[1]:  
                return False
        return True

    def pos_index(worker, n):
        out = []
        for i in range(n):
            s = (i, worker[i])
            out.append(s)
        return out

    worker = [-1 for _ in range(n)] 
    find_pos(0, worker, n)
    index = []
    for t in res:
        index.append(pos_index(t, n))
    return index


if __name__ == '__main__':
    """ 
    Задача решается в три этапа:
    1. Находятся индексы уникальных наборов (работник, задача), чтобы за одну задачу не могли браться двое
    2. формируется список возможных уникальных матриц 8*8 по числу задач, чтобы каждому
    работнику досталась своя задача
    3. По набору уникальных значений, считаем минимальную сумму, которую потратим на каждую задачу. 
    """
    print('start')
    global_min = 0
    global_rows = []
    count_tasks = len(work_table[0]) # всего 8 задач

    # получаем индексы уникальных наборов
    result = get_all_positions(count_tasks)

    for index, matrix in enumerate(combinations(work_table, count_tasks)):
        # создаем матрицу 8*8 для распределения задач по работникам
        # получаем все возможные позиции, задач, которые могут быть взяты
        local_min = 0
        local_rows = []
        # определяем первое значение минимума, с которым будем сравнивать
        for r in result[0]:
            local_min += matrix[r[0]][r[1]]
        # находим минимум среди всех остальных
        for rows in result:
            summ = 0
            for r in rows:
                summ += matrix[r[0]][r[1]]
            if summ < local_min:
                min = summ
                local_rows = rows
        # определеяем начальное значение общего минимума
        if index == 0:
            global_min = local_min
        # находим минимум у всех матриц
        if global_min > local_min:
            global_min = local_min
            global_rows = local_rows

    print('Минимальная сумма: ', global_min)
    print('Распределение задач: ', global_rows)